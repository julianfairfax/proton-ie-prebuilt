#!/bin/bash

export GPG_TTY=$(tty)

echo "$KEY" > priv.gpg
gpg --import pub.gpg && gpg --batch --import priv.gpg

version="ie-1.3.3"

if [[ $(wget https://julianfairfax.codeberg.page/proton-ie-prebuilt/amd64/version.txt -O -) == $version ]]; then
	mkdir -p public/amd64

	cd public/amd64

	wget https://julianfairfax.codeberg.page/proton-ie-prebuilt/amd64/proton-ie

	if [[ ! -f proton-ie ]]; then
		wget https://julianfairfax.codeberg.page/proton-ie-prebuilt/amd64/proton-ie
	fi

	wget https://julianfairfax.codeberg.page/proton-ie-prebuilt/amd64/proton-ie.asc
	
	cd ../../
else
	git clone https://github.com/ProtonMail/proton-bridge

	cd proton-bridge

	git fetch --tags

	git checkout tags/$version

	make build-ie-nogui

	mkdir -p ../public/amd64

	cp ../pub.gpg ../public

	gpg --detach-sig --armor --sign proton-ie

	cp proton-ie ../public/amd64/

	cp proton-ie.asc ../public/amd64/
	
	cd ../
	
	rm -rf proton-bridge
fi

echo "$version" | tee public/amd64/version.txt
	
echo "$version" | tee public/version.txt
