#!/bin/bash

export GPG_TTY=$(tty)

echo "$KEY" > priv.gpg
gpg --import pub.gpg && gpg --batch --import priv.gpg

version="ie-1.3.3"

if [[ $(wget https://julianfairfax.codeberg.page/proton-ie-prebuilt/arm64/version.txt -O -) == $version ]]; then
	mkdir -p public/arm64

	cd public/arm64

	wget https://julianfairfax.codeberg.page/proton-ie-prebuilt/arm64/proton-ie

	if [[ ! -f proton-ie ]]; then
		wget https://julianfairfax.codeberg.page/proton-ie-prebuilt/arm64/proton-ie
	fi

	wget https://julianfairfax.codeberg.page/proton-ie-prebuilt/arm64/proton-ie.asc
	
	cd ../../
else
	git clone https://github.com/ProtonMail/proton-bridge

	cd proton-bridge

	git fetch --tags

	git checkout tags/$version

	GOARCH=arm64 CGO_ENABLED=1 CC=/usr/bin/aarch64-linux-gnu-gcc-12 PKG_CONFIG_PATH=/usr/lib/aarch64-linux-gnu/pkgconfig make build-ie-nogui

	mkdir -p ../public/arm64

	cp ../pub.gpg ../public

	gpg --detach-sig --armor --sign proton-ie

	cp proton-ie ../public/arm64/

	cp proton-ie.asc ../public/arm64/
	
	cd ../
	
	rm -rf proton-bridge
fi

echo "$version" | tee public/arm64/version.txt
